import pandas as pd 
import requests
import os
from decouple import config
from datetime import datetime as dt
import json 
import sys
import time


def policy_collector(model_id, schema_parameter_list):
    user = config("HX_API_USER")
    password = config("HX_API_KEY")

    # get all policy ids relating to a model id
    url =f"https://api.optio.hxrenew.com/api/v1/policies/options/"


    # Unpaged True to get all policies
    params = {
        "model_id": model_id,
        "unpaged":True
    }

    # Call to API to get policy IDs
    try:
        response = requests.get(url, params=params, auth=(user, password))
    except requests.RequestException:
        print("Unable to connect to API")

    # Error handling based on status code returned by API
    if response.ok:
        # results stored in dict : {data:{variable_name: value}}
        result = response.json()
        policy_ids = []
        for policy in result:
            policy_ids.append(policy["id"])

    list_of_policy_jsons = []

    start = dt.now()
    # For each policy id get its snapshot
    for i, policy_id in enumerate(policy_ids,1):

        if i == 1:
            print(f"Collecting policy snapshots from Renew API V1")

        now = dt.now()
        seconds_passed = (now-start).seconds
        if seconds_passed % 5 == 0:
            sys.stdout.write("\r" + f"Snapshot {i} of {len(policy_ids)} ")
            sys.stdout.flush()

        # URL & parameters for the API
        url = f"https://api.optio.hxrenew.com/api/v1/policies/options/{policy_id}/snapshot"
        params = {
            "path": schema_parameter_list
        }

        # Call to API
        try:
            response = requests.get(url, params=params, auth=(user, password))
        except requests.RequestException:
            print("Unable to connect to API")

        # Error handling based on status code returned by API
        if response.ok:
            # results stored in dict : {data:{variable_name: value}}
            result = response.json()
            list_of_policy_jsons.append(result)

    # Save the raw data
    now = dt.now().strftime("%Y-%m-%d_%H-%M-%S")
    parent_dir = os.path.dirname(os.path.abspath(__file__))
    dir_path = os.path.join(parent_dir, "raw_data")
    os.makedirs(dir_path, exist_ok=True)
    file_path = os.path.join(dir_path, f"mb_data_{now}.json")

    with open(file_path, 'w') as f:
        json.dump(list_of_policy_jsons, f)

    return list_of_policy_jsons



def mb_to_excel():


    # Comment out this and uncomment the next line if you want to use an exisiting raw data
    data = policy_collector(12, [
                "/programme",
                "/policy",
                "/pricing",
                "/risk_assessment",
                "/hx_core",
                "/model_parameters",
                "/optio_core",
            ])


    #  Change the path string to the relevant file if using local data
    # with open("raw_data/mb_data_2023-04-05_11-52-34.json", "r") as F:
    #     data = json.load(F)

    policies_data = []
    programme_data = []

    start = dt.now()
    for i,record in enumerate(data, 1):

        if i == 1:
            print(f"extracting policy data to excel")
        now = dt.now()
        seconds_passed = (now-start).seconds
        if seconds_passed % 5 == 0:
            sys.stdout.write("\r" + f"Snapshot {i} of {len(data)} ")
            sys.stdout.flush()

        pol = record["data"]
        try:
            policy_data = {
                #Core
                "id": record["id"],
                "inception_date": pol["hx_core"]["inception_date"],
                "expiry_date": pol["hx_core"]["expiry_date"],
                "is_renewal": pol["optio_core"]["is_renewal"],

                # Policy
                "status" : pol["policy"]["status"],
                "insured_name" : pol["policy"]["insured_name"],
                "retroactive_inception_date" : pol["policy"]["retroactive_inception_date"],
                "region" : pol["policy"]["region"],
                "broker" : pol["policy"]["broker"],
                "underwriter_name" : pol["policy"]["underwriter_name"],
                "profit_type" : pol["policy"]["profit_type"],
                "enterprise_size" : pol["policy"]["enterprise_size"],
                "include_cyber" : pol["policy"]["include_cyber"],
                "facility_type" : pol["policy"]["facility_type"],
                "gross_revenue" : pol["policy"]["gross_revenue"],
                "revenue_collections" : pol["policy"]["revenue_collections"],
                "risk_load" : pol["policy"]["cyber"]["risk_load"],
                "cyber_crime" : pol["policy"]["cyber"]["cyber_crime"],
                "capital_relativity": pol["model_parameters"]["capital_relativity"],



                # pricing - cyber
                "cb_base_premium": pol["pricing"]["cyber"]["base_premium"],
                "cb_base_rate": pol["pricing"]["cyber"]["base_rate"],

                # pricing - med billings
                "mb_base_premium": pol["pricing"]["med_billings"]["base_premium"],
                "mb_base_rate": pol["pricing"]["med_billings"]["base_rate"],

                # risk assessment
                "ra_company_management": pol["risk_assessment"]["company_management"]["selected"],
                "ra_company_management_comment": pol["risk_assessment"]["company_management"]["comment"],

                "ra_risk_management": pol["risk_assessment"]["risk_management"]["selected"],
                "ra_risk_management_comment": pol["risk_assessment"]["risk_management"]["comment"],

                "ra_other_considerations": pol["risk_assessment"]["other_considerations"]["selected"],
                "ra_other_considerations_comment": pol["risk_assessment"]["other_considerations"]["comment"],

                "ra_terms_and_conditions": pol["risk_assessment"]["terms_and_conditions"]["selected"],
                "ra_terms_and_conditions_comment": pol["risk_assessment"]["terms_and_conditions"]["comment"],

                "ra_company_management": pol["risk_assessment"]["company_management"]["selected"],
                "ra_company_management_comment": pol["risk_assessment"]["company_management"]["comment"],

                "ra_total_credit_debit": pol["risk_assessment"]["total_credit_debit"]

            }
            policies_data.append(policy_data)


            for i in range(1,6):
                layer = pol["programme"][f"layer_{i}"]
                if layer["med_billings"]["limit"]:
                    layer_data = {
                        "id": record["id"],
                        "mb_deductible": layer["med_billings"]["deductible"],
                        "mb_deductible_factor": layer["med_billings"]["deductible_factor"],
                        "mb_mechanical_gross_premium": layer["med_billings"]["mechanical_gross_premium"],
                        "mb_technical_gross_premium": layer["med_billings"]["technical_gross_premium"],
                        "mb_agreed_gross_premium": layer["med_billings"]["agreed_gross_premium"],
                        "mb_priced_loss_ratio": layer["med_billings"]["priced_loss_ratio"],
                        "mb_loss_cost": layer["med_billings"]["loss_cost"],
                        "mb_rarc_cash": layer["med_billings"]["rarc"]["cash"],
                        "mb_rarc_loss_cost": layer["med_billings"]["rarc"]["loss_cost"],
                        "mb_rarc_lda": layer["med_billings"]["rarc"]["lda"],
                        "mb_rarc_other": layer["med_billings"]["rarc"]["other"],
                        "mb_rarc_coverage": layer["med_billings"]["rarc"]["coverage"],
                        "mb_rarc_total": layer["med_billings"]["rarc"]["total"],
                        "mb_ilf" : layer["med_billings"]["ilf"],
                        "mb_risk_code_split" : layer["med_billings"]["risk_code_split"],
                        "mb_share" : layer["med_billings"]["share"],
                        "mb_attachment" : layer["med_billings"]["attachment"],
                        "mb_limit" : layer["med_billings"]["limit"],
                        "mb_aggregate_limit" : layer["med_billings"]["aggregate_limit"],
                        "mb_aggregate_limit_factor" : layer["med_billings"]["aggregate_limit_factor"],
                        "mb_risk_code" : layer["med_billings"]["risk_code"],
                        "cb_deductible": layer["cyber"]["deductible"],
                        "cb_deductible_factor": layer["cyber"]["deductible_factor"],
                        "cb_mechanical_gross_premium": layer["cyber"]["mechanical_gross_premium"],
                        "cb_technical_gross_premium": layer["cyber"]["technical_gross_premium"],
                        "cb_agreed_gross_premium": layer["cyber"]["agreed_gross_premium"],
                        "cb_priced_loss_ratio": layer["cyber"]["priced_loss_ratio"],
                        "cb_loss_cost": layer["cyber"]["loss_cost"],
                        "cb_rarc_cash": layer["cyber"]["rarc"]["cash"],
                        "cb_rarc_loss_cost": layer["cyber"]["rarc"]["loss_cost"],
                        "cb_rarc_lda": layer["cyber"]["rarc"]["lda"],
                        "cb_rarc_other": layer["cyber"]["rarc"]["other"],
                        "cb_rarc_coverage": layer["cyber"]["rarc"]["coverage"],
                        "cb_rarc_total": layer["cyber"]["rarc"]["total"],
                        "cb_ilf": layer["cyber"]["ilf"],
                        "cb_risk_code_split": layer["cyber"]["risk_code_split"],
                        "cb_limit": layer["cyber"]["limit"],
                        "cb_risk_code": layer["cyber"]["risk_code"],
                        "policy_reference" : layer["shared"]["policy_reference"],
                        "brokerage" : layer["shared"]["brokerage"],
                        "total_mechanical_gross_premium" : layer["shared"]["mechanical_gross_premium"],
                        "total_technical_gross_premium" : layer["shared"]["technical_gross_premium"],
                        "total_agreed_gross_premium" : layer["shared"]["agreed_gross_premium"],
                        "expiring_policy_reference" : layer["shared"]["expiring_policy_reference"],
                        "total_priced_loss_ratio" : layer["metrics"]["priced_loss_ratio"],
                        "total_loss_cost" : layer["metrics"]["loss_cost"],
                        "total_rate_adequacy_mechanical" : layer["metrics"]["rate_adequacy_mechanical"],
                        "total_rate_adequacy_technical" : layer["metrics"]["rate_adequacy_technical"],
                        "total_rarc" : layer["metrics"]["rarc"],
                        "total_roc" : layer["metrics"]["roc"],
                    }

                    programme_data.append(layer_data)


            

        except TypeError as e:
            id = record["id"]
            print(f"{e}: {id}")


    pol_df = pd.DataFrame(policies_data)
    prog_df = pd.DataFrame(programme_data)
    now = dt.now().strftime("%Y-%m-%d_%H-%M-%S")

    parent_dir = os.path.dirname(os.path.abspath(__file__))
    dir_path = os.path.join(parent_dir, "excel")
    os.makedirs(dir_path, exist_ok=True)
    file_path = os.path.join(dir_path, f"mb_data_{now}.xlsx")

    with pd.ExcelWriter(file_path) as writer:
        pol_df.to_excel(writer, sheet_name="policy", index=False)
        prog_df.to_excel(writer, sheet_name="programme", index=False)



if __name__ == "__main__":
    mb_to_excel()

    

